window.onload = function onLoad() {
  // circle
  var circle = new ProgressBar.Circle('.time-box', {
    color: '#ececec',
    trailColor: '#ececec',
    trailWidth: 1,
    duration: 1400,
    easing: 'bounce',
    strokeWidth: 3,
    from: {
      color: '#ececec',
      a: 0
    },
    to: {
      color: '#0D8F52',
      a: 1
    },
    step: function (state, circle) {
      circle.path.setAttribute('stroke', state.color);
    }
  });
  circle.animate(1.0);
}