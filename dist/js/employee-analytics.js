// Chart Inside Text 
Chart.pluginService.register({
  beforeDraw: function (chart) {
    if (chart.chart.chart.config.type == 'doughnut') {
      var width = chart.chart.width,
        height = chart.chart.height,
        ctx = chart.chart.ctx;
      ctx.restore();
      var fontSize = (height / 100).toFixed(2);
      ctx.font = fontSize + "em Product Sans";
      ctx.textBaseline = "middle";
      // Ensure your first data is the percentage data in your dataset
      var chart_percent = chart.chart.chart.data.datasets[0].data[0];
      chart_percent = chart_percent || 0;
      var text = chart_percent,
        textX = Math.round((width - ctx.measureText(text).width) / 2),
        textY = (height / 2)
      ctx.fillText(text, textX, textY);
      ctx.fillStyle = "#535353";
      ctx.save();
    }
  }
});
// Chart Inside Text Ends

// Pie Chart Start
var ectxPie = document.getElementById('ePieChart1');
var ectxPie1 = document.getElementById('ePieChart2');
var ectxPie2 = document.getElementById('ePieChart3');
var ectxPie3 = document.getElementById('ePieChart4');
var ectxPie4 = document.getElementById('ePieChart5');
var ectxPie5 = document.getElementById('ePieChart6');

var pieChart = new Chart(ectxPie, {
  type: 'doughnut',
  data: {
    labels: ['Aptitude', 'Remining'],
    datasets: [{
      data: [70, 30],
      backgroundColor: [
        'rgba(13, 143, 82, 0.6)',
        'rgba(13, 143, 82, 0.5)',
      ],
      display: true,
      labelString: 'Total Employees',
      borderColor: [
        'rgba(13, 143, 82, 1)',
      ],
      hoverBackgroundColor: ['rgba(13, 143, 82, 0.78)'],
      borderWidth: 1
    }]
  },
  options: {
    circular: true,
    cutoutPercentage: 70,
    legend: {
      display: false
    }
  }
});
var pieChart1 = new Chart(ectxPie1, {
  type: 'doughnut',
  data: {
    labels: ['Timeliness', 'Remining'],
    datasets: [{
      data: [80, 20],
      backgroundColor: [
        'rgba(255, 145, 113, 0.8)',
        'rgba(255, 145, 113, 0.2)',
      ],
      borderColor: [
        'rgba(255, 145, 113, 1)',
      ],
      hoverBackgroundColor: ['rgba(255, 145, 113, 1)'],
      borderWidth: 1
    }]
  },
  options: {
    circular: true,
    cutoutPercentage: 70,
    legend: {
      display: false
    }
  }
});
var pieChart2 = new Chart(ectxPie2, {
  type: 'doughnut',
  data: {
    labels: ['Attitude', 'Remining'],
    datasets: [{
      data: [95, 5],
      backgroundColor: [
        'rgba(255, 113, 113, 0.8)',
        'rgba(255, 113, 113, 0.2)',
      ],
      borderColor: [
        'rgba(255, 113, 113, 1)',
      ],
      hoverBackgroundColor: ['rgba(255, 113, 113, 1)'],
      borderWidth: 1
    }]
  },
  options: {
    circular: true,
    cutoutPercentage: 70,
    legend: {
      display: false
    }
  }
});
var pieChart3 = new Chart(ectxPie3, {
  type: 'doughnut',
  data: {
    labels: ['Cooperation', 'Remining'],
    datasets: [{
      data: [75, 25],
      backgroundColor: [
        'rgba(255, 201, 60, 0.8)',
        'rgba(255, 201, 60, 0.2)',
      ],
      borderColor: [
        'rgba(255, 201, 60, 1)',
      ],
      hoverBackgroundColor: ['rgba(255, 201, 60,  1)'],
      borderWidth: 1
    }]
  },
  options: {
    circular: true,
    cutoutPercentage: 70,
    legend: {
      display: false
    }
  }
});
var pieChart4 = new Chart(ectxPie4, {
  type: 'doughnut',
  data: {
    labels: ['Adaptibility', 'Remining'],
    datasets: [{
      data: [86, 14],
      backgroundColor: [
        'rgba(187, 153, 205, 0.8)',
        'rgba(187, 153, 205, 0.2)',
      ],
      borderColor: [
        'rgba(187, 153, 205, 1)',
      ],
      hoverBackgroundColor: ['rgba(187, 153, 205, 1)'],
      borderWidth: 1
    }]
  },
  options: {
    circular: true,
    cutoutPercentage: 70,
    legend: {
      display: false
    }
  }
});