$(document).ready(function () {
  $(function () {
    $(".ca-check, .employeer-check, .position-check, .salary-check, .clauses-check, .clauses-bottom-check, .termination-check").checkboxradio({
      icon: false
    });
  });

  // General Info
  $(function(){
    $("#gen-others1 label").on('click', function(){
      $("#gen-others-input").addClass('d-none');
    });
    $("#gen-others2 label").on('click', function(){
      $("#gen-others-input").addClass('d-none');
    });
    $("#gen-others3 label").on('click', function(){
      $("#gen-others-input").removeClass('d-none');
    });
  });
  // General Info Ends

  // job details
  $(function(){
    $("#job-yes").on('click', function(){
      $("#startDate").removeClass('d-none');
      $("#endDate").addClass('d-none');
    });
    $("#job-no").on('click', function(){
      $("#startDate").removeClass('d-none');
      $("#endDate").removeClass('d-none');
    });
  });
  // job details ends
  
  // Employer Details
  $(function(){
    $("#form-trigger1").on('click', function(){
      $("#individual-emp").removeClass('d-none');
      $("#company-emp").addClass('d-none');
    });
    $("#form-trigger2").on('click', function(){
      $("#individual-emp").addClass('d-none');
      $("#company-emp").removeClass('d-none');
    });
  });
  // Employer Details ends

  // Salary Type
  $(function () {
    $(".hourly").on('click', function () {
      $(".hourly-input").removeClass("d-none");
      $(".salary-input").addClass("d-none");
      $(".commission-input").addClass("d-none");
      $(".hourlyAndCommission-input").addClass("d-none");
      $(".salaryAndCommission-input").addClass("d-none");
    });
    $(".salary").on('click', function () {
      $(".hourly-input").addClass("d-none");
      $(".salary-input").removeClass("d-none");
      $(".commission-input").addClass("d-none");
      $(".hourlyAndCommission-input").addClass("d-none");
      $(".salaryAndCommission-input").addClass("d-none");
    });
    $(".commission").on('click', function () {
      $(".hourly-input").addClass("d-none");
      $(".salary-input").addClass("d-none");
      $(".commission-input").removeClass("d-none");
      $(".hourlyAndCommission-input").addClass("d-none");
      $(".salaryAndCommission-input").addClass("d-none");
    });
    $(".hourlyAndCommission").on('click', function () {
      $(".hourly-input").addClass("d-none");
      $(".salary-input").addClass("d-none");
      $(".commission-input").addClass("d-none");
      $(".hourlyAndCommission-input").removeClass("d-none");
      $(".salaryAndCommission-input").addClass("d-none");
    });
    $(".salaryAndCommission").on('click', function () {
      $(".hourly-input").addClass("d-none");
      $(".salary-input").addClass("d-none");
      $(".commission-input").addClass("d-none");
      $(".hourlyAndCommission-input").addClass("d-none");
      $(".salaryAndCommission-input").removeClass("d-none");
    });
  });
  // Salary type ends

  // clauses starts
  $(function () {
    $(".confidentiality-top label").on('click', function () {
      $(".confidentiality-bottom").removeClass('d-none');
      $(".solicitation-bottom").addClass('d-none');
      $(".complete-bottom").addClass('d-none');
    });
    $(".solicitation-top label").on('click', function () {
      $(".confidentiality-bottom").addClass('d-none');
      $(".solicitation-bottom").removeClass('d-none');
      $(".complete-bottom").addClass('d-none');
    });
    $(".complete-top label").on('click', function () {
      $(".confidentiality-bottom").addClass('d-none');
      $(".solicitation-bottom").addClass('d-none');
      $(".complete-bottom").removeClass('d-none');
    });
  });
  // Clauses Ends

  // Termination Notice
  $(function () {
    $(".week1").on('click', function () {
      $(".termination-bottom").addClass('d-none')
    });
    $(".week2").on('click', function () {
      $(".termination-bottom").addClass('d-none')
    });
    $(".week3").on('click', function () {
      $(".termination-bottom").addClass('d-none')
    });
    $(".week4").on('click', function () {
      $(".termination-bottom").addClass('d-none')
    });
    $(".week5").on('click', function () {
      $(".termination-bottom").removeClass('d-none');
    });
  });
  $(function () {
    $(".s-week1").on('click', function () {
      $(".termination-bottom2").addClass('d-none')
    });
    $(".s-week2").on('click', function () {
      $(".termination-bottom2").addClass('d-none')
    });
    $(".s-week3").on('click', function () {
      $(".termination-bottom2").addClass('d-none')
    });
    $(".s-week4").on('click', function () {
      $(".termination-bottom2").addClass('d-none')
    });
    $(".s-week5").on('click', function () {
      $(".termination-bottom2").removeClass('d-none');
    });
  });
  // Termination Notice Ends
});