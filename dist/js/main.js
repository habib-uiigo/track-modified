$(window).scroll(function () {
  if ($(window).scrollTop() >= 10) {
    $('header').addClass('fixed-header');
  } else {
    $('header').removeClass('fixed-header');
  }
});

// text editor
$(document).ready(function () {
  $('.summernote').summernote({
    height: 150,
    codemirror: {
      theme: 'monokai'
    }
  });
});
// custom dropdown

$(document).ready(function () {
  //TOGGLING NESTED ul
  $(".drop-down .selected a").click(function () {
    $(".drop-down .options ul").toggle();
  });
});


$(document).ready(function () {
  //SELECT OPTIONS AND HIDE OPTION AFTER SELECTION
  $(".drop-down .options ul li a").click(function () {
    var text = $(this).html();
    $(".drop-down .selected a span").html(text);
    $(".drop-down .options ul").hide();
  });

});

//HIDE OPTIONS IF CLICKED ANYWHERE ELSE ON PAGE
$(document).ready(function () {
  $(document).bind('click', function (e) {
    var $clicked = $(e.target);
    if (!$clicked.parents().hasClass("drop-down"))
      $(".drop-down .options ul").hide();
  });

});
//dashboard

$(document).ready(function () {
  $(function () {

    'use strict';

    var aside = $('.side-nav'),
      showAsideBtn = $('.show-side-btn'),
      contents = $('#contents'),
      _window = $(window)

    showAsideBtn.on("click", function () {
      $("#" + $(this).data('show')).toggleClass('show-side-nav');
      contents.toggleClass('margin');
    });

    if (_window.width() <= 767) {
      aside.addClass('show-side-nav');
    }

    _window.on('resize', function () {
      if ($(window).width() > 767) {
        aside.removeClass('show-side-nav');
      }
    });

    // dropdown menu in the side nav
    var slideNavDropdown = $('.side-nav-dropdown');

    $('.side-nav .categories li').on('click', function () {

      var $this = $(this)

      $this.toggleClass('opend').siblings().removeClass('opend');

      if ($this.hasClass('opend')) {
        $this.find('.side-nav-dropdown').slideToggle('fast');
        $this.siblings().find('.side-nav-dropdown').slideUp('fast');
      } else {
        $this.find('.side-nav-dropdown').slideUp('fast');
      }
    });

    $('.side-nav .close-aside').on('click', function () {
      $('#' + $(this).data('close')).addClass('show-side-nav');
      contents.removeClass('margin');
    });

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });

  });

});
$('[data-toggle="datepicker"]').datepicker({
  autoHide: true
});

$(document).ready(function () {
  $(function () {
    $("#progressbar").progressbar({
      value: 37
    });
  });
});

var swiper = new Swiper('.swiper-container', {
  slidesPerView: 4,
  spaceBetween: 30,
  loop: true,
  speed: 1600,
  pagination: {
    el: '.swiper-pagination',
    dynamicBullets: true,
  },
  autoplay: {
    delay: 0,
  },
  breakpoints: {
    1600: {
      slidesPerView: 4,
      spaceBetween: 20,
    },
    1200: {
      slidesPerView: 3,
      spaceBetween: 20,
    },
    575: {
      slidesPerView: 2,
      spaceBetween: 20,
    },
    400: {
      slidesPerView: 1,
      spaceBetween: 20,
    },
  }
});