// Chart Inside Text 
Chart.pluginService.register({
  beforeDraw: function (chart) {
    if (chart.chart.chart.config.type == 'doughnut') {
      var width = chart.chart.width,
        height = chart.chart.height,
        ctx = chart.chart.ctx;
      ctx.restore();
      var fontSize = (height / 100).toFixed(2);
      ctx.font = fontSize + "em Product Sans";
      ctx.textBaseline = "middle";
      // Ensure your first data is the percentage data in your dataset
      var chart_percent = chart.chart.chart.data.datasets[0].data[0];
      chart_percent = chart_percent || 0;
      var text = chart_percent,
        textX = Math.round((width - ctx.measureText(text).width) / 2),
        textY = (height / 2)
      ctx.fillText(text, textX, textY);
      ctx.fillStyle = "#535353";
      ctx.save();
    }
  }
});

var promo1 = document.getElementById('promo-1');
var promo2 = document.getElementById('promo-2');
var promo3 = document.getElementById('promo-3');
var promo4 = document.getElementById('promo-4');
var promo5 = document.getElementById('promo-5');

var promoVar1 = new Chart(promo1, {
  type: 'doughnut',
  data: {
    labels: ['Completed', 'Remining'],
    datasets: [{
      data: [5, 2],
      backgroundColor: [
        'rgba(13, 143, 82, 0.7)',
        'rgba(13, 143, 82, 0.6)',
      ],
      display: true,
      labelString: 'Total Promoted',
      borderColor: [
        'rgba(13, 143, 82, 1)',
      ],
      hoverBackgroundColor: ['rgba(13, 143, 82, 0.5)'],
      borderWidth: 1
    }]
  },
  options: {
    circular: true,
    cutoutPercentage: 70,
    legend: {
      display: false
    }
  }
});
var promoVar2 = new Chart(promo2, {
  type: 'doughnut',
  data: {
    labels: ['Completed', 'Remining'],
    datasets: [{
      data: [6, 1],
      backgroundColor: [
        'rgba(13, 143, 82, 0.4)',
        'rgba(13, 143, 82, 0.2)',
      ],
      display: true,
      labelString: 'Total Promoted',
      borderColor: [
        'rgba(13, 143, 82, 1)',
      ],
      hoverBackgroundColor: ['rgba(13, 143, 82, 0.5)'],
      borderWidth: 1
    }]
  },
  options: {
    circular: true,
    cutoutPercentage: 70,
    legend: {
      display: false
    }
  }
});
var promoVar3 = new Chart(promo3, {
  type: 'doughnut',
  data: {
    labels: ['Completed', 'Remining'],
    datasets: [{
      data: [3, 4],
      backgroundColor: [
        'rgba(13, 143, 82, 0.7)',
        'rgba(13, 143, 82, 0.6)',
      ],
      display: true,
      labelString: 'Total Promoted',
      borderColor: [
        'rgba(13, 143, 82, 1)',
      ],
      hoverBackgroundColor: ['rgba(13, 143, 82, 0.5)'],
      borderWidth: 1
    }]
  },
  options: {
    circular: true,
    cutoutPercentage: 70,
    legend: {
      display: false
    }
  }
});
var promoVar4 = new Chart(promo4, {
  type: 'doughnut',
  data: {
    labels: ['Completed', 'Remining'],
    datasets: [{
      data: [3, 3],
      backgroundColor: [
        'rgba(13, 143, 82, 0.4)',
        'rgba(13, 143, 82, 0.2)',
      ],
      display: true,
      labelString: 'Total Promoted',
      borderColor: [
        'rgba(13, 143, 82, 1)',
      ],
      hoverBackgroundColor: ['rgba(13, 143, 82, 0.5)'],
      borderWidth: 1
    }]
  },
  options: {
    circular: true,
    cutoutPercentage: 70,
    legend: {
      display: false
    }
  }
});
var promoVar5 = new Chart(promo5, {
  type: 'doughnut',
  data: {
    labels: ['Completed', 'Remining'],
    datasets: [{
      data: [4, 3],
      backgroundColor: [
        'rgba(13, 143, 82, 0.7)',
        'rgba(13, 143, 82, 0.6)',
      ],
      display: true,
      labelString: 'Total Promoted',
      borderColor: [
        'rgba(13, 143, 82, 1)',
      ],
      hoverBackgroundColor: ['rgba(13, 143, 82, 0.5)'],
      borderWidth: 1
    }]
  },
  options: {
    circular: true,
    cutoutPercentage: 70,
    legend: {
      display: false
    }
  }
});