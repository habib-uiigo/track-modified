(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
    typeof define === 'function' && define.amd ? define(['exports'], factory) :
    (global = global || self, factory(global.adminlte = {}));
}(this, function (exports) {
  'use strict';

  /**
   * --------------------------------------------
   * AdminLTE ControlSidebar.js
   * License MIT
   * --------------------------------------------
   */
  var ControlSidebar = function ($) {
    /**
     * Constants
     * ====================================================
     */
    var NAME = 'ControlSidebar';
    var DATA_KEY = 'lte.controlsidebar';
    var EVENT_KEY = "." + DATA_KEY;
    var JQUERY_NO_CONFLICT = $.fn[NAME];
    var Event = {
      COLLAPSED: "collapsed" + EVENT_KEY,
      EXPANDED: "expanded" + EVENT_KEY
    };
    var Selector = {
      CONTROL_SIDEBAR: '.control-sidebar',
      CONTROL_SIDEBAR_CONTENT: '.control-sidebar-content',
      DATA_TOGGLE: '[data-widget="control-sidebar"]',
      CONTENT: '.content-wrapper',
      HEADER: '.main-header',
      FOOTER: '.main-footer'
    };
    var ClassName = {
      CONTROL_SIDEBAR_ANIMATE: 'control-sidebar-animate',
      CONTROL_SIDEBAR_OPEN: 'control-sidebar-open',
      CONTROL_SIDEBAR_SLIDE: 'control-sidebar-slide-open',
      LAYOUT_FIXED: 'layout-fixed',
      NAVBAR_FIXED: 'layout-navbar-fixed',
      NAVBAR_SM_FIXED: 'layout-sm-navbar-fixed',
      NAVBAR_MD_FIXED: 'layout-md-navbar-fixed',
      NAVBAR_LG_FIXED: 'layout-lg-navbar-fixed',
      NAVBAR_XL_FIXED: 'layout-xl-navbar-fixed',
      FOOTER_FIXED: 'layout-footer-fixed',
      FOOTER_SM_FIXED: 'layout-sm-footer-fixed',
      FOOTER_MD_FIXED: 'layout-md-footer-fixed',
      FOOTER_LG_FIXED: 'layout-lg-footer-fixed',
      FOOTER_XL_FIXED: 'layout-xl-footer-fixed'
    };
    /**
     * Class Definition
     * ====================================================
     */

    var ControlSidebar =
      /*#__PURE__*/
      function () {
        function ControlSidebar(element, config) {
          this._element = element;
          this._config = config;

          this._init();
        } // Public


        var _proto = ControlSidebar.prototype;

        _proto.show = function show() {
          // Show the control sidebar
          if (this._config.controlsidebarSlide) {
            $('html').addClass(ClassName.CONTROL_SIDEBAR_ANIMATE);
            $('body').removeClass(ClassName.CONTROL_SIDEBAR_SLIDE).delay(300).queue(function () {
              $(Selector.CONTROL_SIDEBAR).hide();
              $('html').removeClass(ClassName.CONTROL_SIDEBAR_ANIMATE);
              $(this).dequeue();
            });
          } else {
            $('body').removeClass(ClassName.CONTROL_SIDEBAR_OPEN);
          }

          var expandedEvent = $.Event(Event.EXPANDED);
          $(this._element).trigger(expandedEvent);
        };

        _proto.collapse = function collapse() {
          // Collapse the control sidebar
          if (this._config.controlsidebarSlide) {
            $('html').addClass(ClassName.CONTROL_SIDEBAR_ANIMATE);
            $(Selector.CONTROL_SIDEBAR).show().delay(10).queue(function () {
              $('body').addClass(ClassName.CONTROL_SIDEBAR_SLIDE).delay(300).queue(function () {
                $('html').removeClass(ClassName.CONTROL_SIDEBAR_ANIMATE);
                $(this).dequeue();
              });
              $(this).dequeue();
            });
          } else {
            $('body').addClass(ClassName.CONTROL_SIDEBAR_OPEN);
          }

          var collapsedEvent = $.Event(Event.COLLAPSED);
          $(this._element).trigger(collapsedEvent);
        };

        _proto.toggle = function toggle() {
          var shouldOpen = $('body').hasClass(ClassName.CONTROL_SIDEBAR_OPEN) || $('body').hasClass(ClassName.CONTROL_SIDEBAR_SLIDE);

          if (shouldOpen) {
            // Open the control sidebar
            this.show();
          } else {
            // Close the control sidebar
            this.collapse();
          }
        } // Private
        ;

        _proto._init = function _init() {
          var _this = this;

          this._fixHeight();

          this._fixScrollHeight();

          $(window).resize(function () {
            _this._fixHeight();

            _this._fixScrollHeight();
          });
          $(window).scroll(function () {
            if ($('body').hasClass(ClassName.CONTROL_SIDEBAR_OPEN) || $('body').hasClass(ClassName.CONTROL_SIDEBAR_SLIDE)) {
              _this._fixScrollHeight();
            }
          });
        };

        _proto._fixScrollHeight = function _fixScrollHeight() {
          var heights = {
            scroll: $(document).height(),
            window: $(window).height(),
            header: $(Selector.HEADER).outerHeight(),
            footer: $(Selector.FOOTER).outerHeight()
          };
          var positions = {
            bottom: Math.abs(heights.window + $(window).scrollTop() - heights.scroll),
            top: $(window).scrollTop()
          };
          var navbarFixed = false;
          var footerFixed = false;

          if ($('body').hasClass(ClassName.LAYOUT_FIXED)) {
            if ($('body').hasClass(ClassName.NAVBAR_FIXED) || $('body').hasClass(ClassName.NAVBAR_SM_FIXED) || $('body').hasClass(ClassName.NAVBAR_MD_FIXED) || $('body').hasClass(ClassName.NAVBAR_LG_FIXED) || $('body').hasClass(ClassName.NAVBAR_XL_FIXED)) {
              if ($(Selector.HEADER).css("position") === "fixed") {
                navbarFixed = true;
              }
            }

            if ($('body').hasClass(ClassName.FOOTER_FIXED) || $('body').hasClass(ClassName.FOOTER_SM_FIXED) || $('body').hasClass(ClassName.FOOTER_MD_FIXED) || $('body').hasClass(ClassName.FOOTER_LG_FIXED) || $('body').hasClass(ClassName.FOOTER_XL_FIXED)) {
              if ($(Selector.FOOTER).css("position") === "fixed") {
                footerFixed = true;
              }
            }

            if (positions.top === 0 && positions.bottom === 0) {
              $(Selector.CONTROL_SIDEBAR).css('bottom', heights.footer);
              $(Selector.CONTROL_SIDEBAR).css('top', heights.header);
              $(Selector.CONTROL_SIDEBAR + ', ' + Selector.CONTROL_SIDEBAR + ' ' + Selector.CONTROL_SIDEBAR_CONTENT).css('height', heights.window - (heights.header + heights.footer));
            } else if (positions.bottom <= heights.footer) {
              if (footerFixed === false) {
                $(Selector.CONTROL_SIDEBAR).css('bottom', heights.footer - positions.bottom);
                $(Selector.CONTROL_SIDEBAR + ', ' + Selector.CONTROL_SIDEBAR + ' ' + Selector.CONTROL_SIDEBAR_CONTENT).css('height', heights.window - (heights.footer - positions.bottom));
              } else {
                $(Selector.CONTROL_SIDEBAR).css('bottom', heights.footer);
              }
            } else if (positions.top <= heights.header) {
              if (navbarFixed === false) {
                $(Selector.CONTROL_SIDEBAR).css('top', heights.header - positions.top);
                $(Selector.CONTROL_SIDEBAR + ', ' + Selector.CONTROL_SIDEBAR + ' ' + Selector.CONTROL_SIDEBAR_CONTENT).css('height', heights.window - (heights.header - positions.top));
              } else {
                $(Selector.CONTROL_SIDEBAR).css('top', heights.header);
              }
            } else {
              if (navbarFixed === false) {
                $(Selector.CONTROL_SIDEBAR).css('top', 0);
                $(Selector.CONTROL_SIDEBAR + ', ' + Selector.CONTROL_SIDEBAR + ' ' + Selector.CONTROL_SIDEBAR_CONTENT).css('height', heights.window);
              } else {
                $(Selector.CONTROL_SIDEBAR).css('top', heights.header);
              }
            }
          }
        };

        _proto._fixHeight = function _fixHeight() {
          var heights = {
            window: $(window).height(),
            header: $(Selector.HEADER).outerHeight(),
            footer: $(Selector.FOOTER).outerHeight()
          };

          if ($('body').hasClass(ClassName.LAYOUT_FIXED)) {
            var sidebarHeight = heights.window - heights.header;

            if ($('body').hasClass(ClassName.FOOTER_FIXED) || $('body').hasClass(ClassName.FOOTER_SM_FIXED) || $('body').hasClass(ClassName.FOOTER_MD_FIXED) || $('body').hasClass(ClassName.FOOTER_LG_FIXED) || $('body').hasClass(ClassName.FOOTER_XL_FIXED)) {
              if ($(Selector.FOOTER).css("position") === "fixed") {
                sidebarHeight = heights.window - heights.header - heights.footer;
              }
            }

            $(Selector.CONTROL_SIDEBAR + ' ' + Selector.CONTROL_SIDEBAR_CONTENT).css('height', sidebarHeight);

            if (typeof $.fn.overlayScrollbars !== 'undefined') {
              $(Selector.CONTROL_SIDEBAR + ' ' + Selector.CONTROL_SIDEBAR_CONTENT).overlayScrollbars({
                className: this._config.scrollbarTheme,
                sizeAutoCapable: true,
                scrollbars: {
                  autoHide: this._config.scrollbarAutoHide,
                  clickScrolling: true
                }
              });
            }
          }
        } // Static
        ;

        ControlSidebar._jQueryInterface = function _jQueryInterface(operation) {
          return this.each(function () {
            var data = $(this).data(DATA_KEY);

            if (!data) {
              data = new ControlSidebar(this, $(this).data());
              $(this).data(DATA_KEY, data);
            }

            if (data[operation] === 'undefined') {
              throw new Error(operation + " is not a function");
            }

            data[operation]();
          });
        };

        return ControlSidebar;
      }();
    /**
     *
     * Data Api implementation
     * ====================================================
     */


    $(document).on('click', Selector.DATA_TOGGLE, function (event) {
      event.preventDefault();

      ControlSidebar._jQueryInterface.call($(this), 'toggle');
    });
    /**
     * jQuery API
     * ====================================================
     */

    $.fn[NAME] = ControlSidebar._jQueryInterface;
    $.fn[NAME].Constructor = ControlSidebar;

    $.fn[NAME].noConflict = function () {
      $.fn[NAME] = JQUERY_NO_CONFLICT;
      return ControlSidebar._jQueryInterface;
    };

    return ControlSidebar;
  }(jQuery);

  var PushMenu = function ($) {
    /**
     * Constants
     * ====================================================
     */
    var NAME = 'PushMenu';
    var DATA_KEY = 'lte.pushmenu';
    var EVENT_KEY = "." + DATA_KEY;
    var JQUERY_NO_CONFLICT = $.fn[NAME];
    var Event = {
      COLLAPSED: "collapsed" + EVENT_KEY,
      SHOWN: "shown" + EVENT_KEY
    };
    var Default = {
      autoCollapseSize: 992,
      enableRemember: false,
      noTransitionAfterReload: true
    };
    var Selector = {
      TOGGLE_BUTTON: '[data-widget="pushmenu"]',
      SIDEBAR_MINI: '.sidebar-mini',
      SIDEBAR_COLLAPSED: '.sidebar-collapse',
      BODY: 'body',
      OVERLAY: '#sidebar-overlay',
      WRAPPER: '.wrapper'
    };
    var ClassName = {
      SIDEBAR_OPEN: 'sidebar-open',
      COLLAPSED: 'sidebar-collapse',
      OPEN: 'sidebar-open'
    };
    /**
     * Class Definition
     * ====================================================
     */

    var PushMenu =
      /*#__PURE__*/
      function () {
        function PushMenu(element, options) {
          this._element = element;
          this._options = $.extend({}, Default, options);

          if (!$(Selector.OVERLAY).length) {
            this._addOverlay();
          }

          this._init();
        } // Public


        var _proto = PushMenu.prototype;

        _proto.show = function show() {
          if (this._options.autoCollapseSize) {
            if ($(window).width() <= this._options.autoCollapseSize) {
              $(Selector.BODY).addClass(ClassName.OPEN);
            }
          }

          $(Selector.BODY).removeClass(ClassName.COLLAPSED);

          if (this._options.enableRemember) {
            localStorage.setItem("remember" + EVENT_KEY, ClassName.OPEN);
          }

          var shownEvent = $.Event(Event.SHOWN);
          $(this._element).trigger(shownEvent);
        };

        _proto.collapse = function collapse() {
          if (this._options.autoCollapseSize) {
            if ($(window).width() <= this._options.autoCollapseSize) {
              $(Selector.BODY).removeClass(ClassName.OPEN);
            }
          }

          $(Selector.BODY).addClass(ClassName.COLLAPSED);

          if (this._options.enableRemember) {
            localStorage.setItem("remember" + EVENT_KEY, ClassName.COLLAPSED);
          }

          var collapsedEvent = $.Event(Event.COLLAPSED);
          $(this._element).trigger(collapsedEvent);
        };

        _proto.toggle = function toggle() {
          if (!$(Selector.BODY).hasClass(ClassName.COLLAPSED)) {
            this.collapse();
          } else {
            this.show();
          }
        };

        _proto.autoCollapse = function autoCollapse(resize) {
          if (resize === void 0) {
            resize = false;
          }

          if (this._options.autoCollapseSize) {
            if ($(window).width() <= this._options.autoCollapseSize) {
              if (!$(Selector.BODY).hasClass(ClassName.OPEN)) {
                this.collapse();
              }
            } else if (resize == true) {
              if ($(Selector.BODY).hasClass(ClassName.OPEN)) {
                $(Selector.BODY).removeClass(ClassName.OPEN);
              }
            }
          }
        };

        _proto.remember = function remember() {
          if (this._options.enableRemember) {
            var toggleState = localStorage.getItem("remember" + EVENT_KEY);

            if (toggleState == ClassName.COLLAPSED) {
              if (this._options.noTransitionAfterReload) {
                $("body").addClass('hold-transition').addClass(ClassName.COLLAPSED).delay(50).queue(function () {
                  $(this).removeClass('hold-transition');
                  $(this).dequeue();
                });
              } else {
                $("body").addClass(ClassName.COLLAPSED);
              }
            } else {
              if (this._options.noTransitionAfterReload) {
                $("body").addClass('hold-transition').removeClass(ClassName.COLLAPSED).delay(50).queue(function () {
                  $(this).removeClass('hold-transition');
                  $(this).dequeue();
                });
              } else {
                $("body").removeClass(ClassName.COLLAPSED);
              }
            }
          }
        } // Private
        ;

        _proto._init = function _init() {
          var _this = this;

          this.remember();
          this.autoCollapse();
          $(window).resize(function () {
            _this.autoCollapse(true);
          });
        };

        _proto._addOverlay = function _addOverlay() {
          var _this2 = this;

          var overlay = $('<div />', {
            id: 'sidebar-overlay'
          });
          overlay.on('click', function () {
            _this2.collapse();
          });
          $(Selector.WRAPPER).append(overlay);
        } // Static
        ;

        PushMenu._jQueryInterface = function _jQueryInterface(operation) {
          return this.each(function () {
            var data = $(this).data(DATA_KEY);

            var _options = $.extend({}, Default, $(this).data());

            if (!data) {
              data = new PushMenu(this, _options);
              $(this).data(DATA_KEY, data);
            }

            if (operation === 'toggle') {
              data[operation]();
            }
          });
        };

        return PushMenu;
      }();
    /**
     * Data API
     * ====================================================
     */


    $(document).on('click', Selector.TOGGLE_BUTTON, function (event) {
      event.preventDefault();
      var button = event.currentTarget;

      if ($(button).data('widget') !== 'pushmenu') {
        button = $(button).closest(Selector.TOGGLE_BUTTON);
      }

      PushMenu._jQueryInterface.call($(button), 'toggle');
    });
    $(window).on('load', function () {
      PushMenu._jQueryInterface.call($(Selector.TOGGLE_BUTTON));
    });
    /**
     * jQuery API
     * ====================================================
     */

    $.fn[NAME] = PushMenu._jQueryInterface;
    $.fn[NAME].Constructor = PushMenu;

    $.fn[NAME].noConflict = function () {
      $.fn[NAME] = JQUERY_NO_CONFLICT;
      return PushMenu._jQueryInterface;
    };

    return PushMenu;
  }(jQuery);

  var Treeview = function ($) {
    /**
     * Constants
     * ====================================================
     */
    var NAME = 'Treeview';
    var DATA_KEY = 'lte.treeview';
    var EVENT_KEY = "." + DATA_KEY;
    var JQUERY_NO_CONFLICT = $.fn[NAME];
    var Event = {
      SELECTED: "selected" + EVENT_KEY,
      EXPANDED: "expanded" + EVENT_KEY,
      COLLAPSED: "collapsed" + EVENT_KEY,
      LOAD_DATA_API: "load" + EVENT_KEY
    };
    var Selector = {
      LI: '.nav-item',
      LINK: '.nav-link',
      TREEVIEW_MENU: '.nav-treeview',
      OPEN: '.menu-open',
      DATA_WIDGET: '[data-widget="treeview"]'
    };
    var ClassName = {
      LI: 'nav-item',
      LINK: 'nav-link',
      TREEVIEW_MENU: 'nav-treeview',
      OPEN: 'menu-open'
    };
    var Default = {
      trigger: Selector.DATA_WIDGET + " " + Selector.LINK,
      animationSpeed: 300,
      accordion: true
    };
    /**
     * Class Definition
     * ====================================================
     */

    var Treeview =
      /*#__PURE__*/
      function () {
        function Treeview(element, config) {
          this._config = config;
          this._element = element;
        } // Public


        var _proto = Treeview.prototype;

        _proto.init = function init() {
          this._setupListeners();
        };

        _proto.expand = function expand(treeviewMenu, parentLi) {
          var _this = this;

          var expandedEvent = $.Event(Event.EXPANDED);

          if (this._config.accordion) {
            var openMenuLi = parentLi.siblings(Selector.OPEN).first();
            var openTreeview = openMenuLi.find(Selector.TREEVIEW_MENU).first();
            this.collapse(openTreeview, openMenuLi);
          }

          treeviewMenu.stop().slideDown(this._config.animationSpeed, function () {
            parentLi.addClass(ClassName.OPEN);
            $(_this._element).trigger(expandedEvent);
          });
        };

        _proto.collapse = function collapse(treeviewMenu, parentLi) {
          var _this2 = this;

          var collapsedEvent = $.Event(Event.COLLAPSED);
          treeviewMenu.stop().slideUp(this._config.animationSpeed, function () {
            parentLi.removeClass(ClassName.OPEN);
            $(_this2._element).trigger(collapsedEvent);
            treeviewMenu.find(Selector.OPEN + " > " + Selector.TREEVIEW_MENU).slideUp();
            treeviewMenu.find(Selector.OPEN).removeClass(ClassName.OPEN);
          });
        };

        _proto.toggle = function toggle(event) {
          var $relativeTarget = $(event.currentTarget);
          var $parent = $relativeTarget.parent();
          var treeviewMenu = $parent.find('> ' + Selector.TREEVIEW_MENU);

          if (!treeviewMenu.is(Selector.TREEVIEW_MENU)) {
            if (!$parent.is(Selector.LI)) {
              treeviewMenu = $parent.parent().find('> ' + Selector.TREEVIEW_MENU);
            }

            if (!treeviewMenu.is(Selector.TREEVIEW_MENU)) {
              return;
            }
          }

          event.preventDefault();
          var parentLi = $relativeTarget.parents(Selector.LI).first();
          var isOpen = parentLi.hasClass(ClassName.OPEN);

          if (isOpen) {
            this.collapse($(treeviewMenu), parentLi);
          } else {
            this.expand($(treeviewMenu), parentLi);
          }
        } // Private
        ;

        _proto._setupListeners = function _setupListeners() {
          var _this3 = this;

          $(document).on('click', this._config.trigger, function (event) {
            _this3.toggle(event);
          });
        } // Static
        ;

        Treeview._jQueryInterface = function _jQueryInterface(config) {
          return this.each(function () {
            var data = $(this).data(DATA_KEY);

            var _config = $.extend({}, Default, $(this).data());

            if (!data) {
              data = new Treeview($(this), _config);
              $(this).data(DATA_KEY, data);
            }

            if (config === 'init') {
              data[config]();
            }
          });
        };

        return Treeview;
      }();
    /**
     * Data API
     * ====================================================
     */


    $(window).on(Event.LOAD_DATA_API, function () {
      $(Selector.DATA_WIDGET).each(function () {
        Treeview._jQueryInterface.call($(this), 'init');
      });
    });
    /**
     * jQuery API
     * ====================================================
     */

    $.fn[NAME] = Treeview._jQueryInterface;
    $.fn[NAME].Constructor = Treeview;

    $.fn[NAME].noConflict = function () {
      $.fn[NAME] = JQUERY_NO_CONFLICT;
      return Treeview._jQueryInterface;
    };

    return Treeview;
  }(jQuery);
}));
