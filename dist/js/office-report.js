var officeBar = document.getElementById('officeBar').getContext('2d');

var officeChart1 = new Chart(officeBar, {
  type: 'bar',
  data: {
    labels: ["1", "5", "10", "15", "20", "25", "31"],
    datasets: [{
        label: 'Intern',
        data: [2, 3, 4, 1, 10, 2, 7],
        borderColor: 'rgba(187, 153, 205, 1)',
        backgroundColor: 'rgba(187, 153, 205, 0.6)',
        borderWidth: 1,
      },
      {
        label: 'Front End Developer',
        data: [10, 3, 4, 2, 7, 9, 10],
        borderColor: 'rgba(255, 201, 60, 1)',
        backgroundColor: 'rgba(255, 201, 60, 0.6)',
        borderWidth: 1,
      },
      {
        label: 'Back End Developer',
        data: [3, 4, 6, 8, 5, 4, 1],
        borderColor: 'rgba(255, 145, 113, 1)',
        backgroundColor: 'rgba(255, 145, 113, 0.6)',
        borderWidth: 1,
      },
      {
        label: 'Full Stack Developer',
        data: [3, 5, 7, 9, 5, 6, 6],
        borderColor: 'rgba(13, 143, 82, 1)',
        backgroundColor: 'rgba(13, 143, 82, 0.6)',
        borderWidth: 1,
      },
    ]
  },
  options: {
    legend: {
      display: false,
    },
    scales: {
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Employees'
        }
      }],
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Dates'
        }
      }],
    }
  }
});

// Line Chart Starts
var attendanceLine = document.getElementById('attendanceLineChart').getContext('2d');

var attendanceLineChart = new Chart(attendanceLine, {
  type: 'line',
  data: {
    labels: ["1", "5", "10", "15", "20", "25", "31"],
    datasets: [{
        label: 'Attended Female Employees',
        data: [75, 74, 40, 55, 60, 70, 71],
        borderWidth: 2,
        borderColor: 'rgba(187, 153, 205, 1)',
        backgroundColor: 'rgba(187, 153, 205, 0.6)',
        borderWidth: 1,
        radius: 6,
        hoverRadius: 8,
        hoverBorderWidth: 1,
        fill: false,
        lineTension: 0.5,
        responsive: true
      },
      {
        label: 'Attended Male Employees',
        data: [50, 55, 40, 33, 22, 28, 40],
        borderWidth: 2,
        borderColor: 'rgba(255, 201, 60, 1)',
        backgroundColor: 'rgba(255, 201, 60, 0.6)',
        borderWidth: 1,
        radius: 6,
        hoverRadius: 8,
        hoverBorderWidth: 1,
        fill: false,
        lineTension: 0.5,
      }
    ]
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: false
        },
        scaleLabel: {
          display: true,
          labelString: 'Employees'
        }
      }],
      xAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Dates'
        }
      }],
    },
    legend: {
      display: false,
    }
  }
});
// Line Chart Ends