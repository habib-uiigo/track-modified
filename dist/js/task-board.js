$(function () {
  $("div.droptrue").sortable({
    connectWith: "div",
    placeholder: "ui-state-highlight"
  });
  $("#sortable2, #sortable3, #sortable4, #sortable5").draggable();

  $("#slider-range-min").slider({
    range: "min",
    value: 0,
    min: 0,
    max: 100,
    slide: function (event, ui) {
      $("#amount").val(ui.value + "%");
    }
  });
  $("#amount").val($("#slider-range-min").slider("value") + "%");
});