$(document).ready(function () {
  $('.description').on('dblclick touchstart', function (e) {
    e.stopPropagation();
    $('.description-text-editor').addClass('d-block').removeClass('d-none');
    $('.description-content').addClass('d-none').removeClass('d-block');
    return false;
  })
  $('.text-editor-add, .text-editor-cancel').on('click touchstart', function (e) {
    e.stopPropagation();
    $('.description-text-editor').addClass('d-none').removeClass('d-block');
    $('.description-content').addClass('d-block').removeClass('d-none');
    return false;
  })
});