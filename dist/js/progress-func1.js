window.onload = function onLoad() {
  // project progress 1
  var projectLine = new ProgressBar.Line('.project-progress-1', {
    color: '#0D8F52',
    trailWidth: 1,
    strokeWidth: 1,
  });
  projectLine.animate(.56);
  // project progress 2
  var projectLine = new ProgressBar.Line('.project-progress-2', {
    color: '#0D8F52',
    trailWidth: 1,
    strokeWidth: 1,
  });
  projectLine.animate(.65);
  // project progress 3
  var projectLine = new ProgressBar.Line('.project-progress-3', {
    color: '#0D8F52',
    trailWidth: 1,
    strokeWidth: 1,
  });
  projectLine.animate(.25);

  // project progress 4
  var projectLine = new ProgressBar.Line('.project-progress-4', {
    color: '#0D8F52',
    trailWidth: 1,
    strokeWidth: 1,
  });
  projectLine.animate(.35);
  // project progress 5
  var projectLine = new ProgressBar.Line('.project-progress-5', {
    color: '#0D8F52',
    trailWidth: 1,
    strokeWidth: 1,
  });
  projectLine.animate(.90);
}