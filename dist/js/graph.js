window.onload = function () {

  var options = {
    animationEnabled: true,
    theme: 'light2',
    title: {
      text: "Monthly Sales - 2020",
      fontColor: '#0D8F52',
      fontSize: 20,
    },
    axisX: {
      valueFormatString: "MMM"
    },
    axisY: {
      title: "Sales (in USD)",
      prefix: "$",
      includeZero: false
    },
    data: [{
      yValueFormatString: "$#,###",
      xValueFormatString: "MMMM",
      type: "spline",
      lineColor: '#0D8F52',
      dataPoints: [{
          x: new Date(2020, 0),
          y: 25060
        },
        {
          x: new Date(2020, 1),
          y: 27980
        },
        {
          x: new Date(2020, 2),
          y: 33800
        },
        {
          x: new Date(2020, 3),
          y: 49400
        },
        {
          x: new Date(2020, 4),
          y: 40260
        },
        {
          x: new Date(2020, 5),
          y: 33900
        },
        {
          x: new Date(2020, 6),
          y: 48000
        },
        {
          x: new Date(2020, 7),
          y: 31500
        },
        {
          x: new Date(2020, 8),
          y: 32300
        },
        {
          x: new Date(2020, 9),
          y: 42000
        },
        {
          x: new Date(2020, 10),
          y: 52160
        },
        {
          x: new Date(2020, 11),
          y: 49400
        }
      ]
    }]
  };
  $("#chartContainer").CanvasJSChart(options);

  var chart = new CanvasJS.Chart("chartContainer1", {
    animationEnabled: true,
    zoomEnabled: true,
    theme: "light2",
    title: {
      text: "Employee Growth From 2015 to 2020",
      fontColor: "#535353",
      fontSize: 20,
      fontWeight: 'bold'
    },
    backgroundColor: "#fff",
    axisX: {
      title: "Year",
      valueFormatString: "####",
      interval: 3,
      fontColor: "#0D8F52"
    },
    axisY: {
      logarithmic: true, //change it to false
      title: "Profit in USD (Log)",
      prefix: "$",
      titleFontColor: "#0D8F52",
      lineColor: "#0D8F52",
      gridThickness: 0,
      lineThickness: 1,
      //includeZero: false,
      labelFormatter: addSymbols
    },
    axisY2: {
      title: "Profit in USD",
      prefix: "$",
      titleFontColor: "#b1b1b1",
      logarithmic: false, //change it to true
      lineColor: "#b1b1b1",
      fontColor: "#535353",
      gridThickness: 0,
      lineThickness: 1,
      labelFormatter: addSymbols
    },
    legend: {
      verticalAlign: "top",
      fontSize: 16,
      fontColor: '#b1b1b1',
      dockInsidePlotArea: true
    },
    data: [{
        type: "line",
        xValueFormatString: "####",
        yValueFormatString: "$#,##0.##",
        showInLegend: true,
        name: "Log Scale",
        dataPoints: [{
            x: 2015,
            y: 8000
          },
          {
            x: 2016,
            y: 20000
          },
          {
            x: 2017,
            y: 40000
          },
          {
            x: 2018,
            y: 60000
          },
          {
            x: 2019,
            y: 90000
          },
          {
            x: 2020,
            y: 140000
          }
        ]
      },
      {
        type: "line",
        xValueFormatString: "####",
        yValueFormatString: "$#,##0.##",
        axisYType: "secondary",
        showInLegend: true,
        name: "Linear Scale",
        dataPoints: [{
            x: 2015,
            y: 8000
          },
          {
            x: 2016,
            y: 20000
          },
          {
            x: 2017,
            y: 40000
          },
          {
            x: 2018,
            y: 60000
          },
          {
            x: 2019,
            y: 90000
          },
          {
            x: 2020,
            y: 140000
          }
        ]
      }
    ]
  });
  chart.render();

  function addSymbols(e) {
    var suffixes = ["", "K", "M", "B"];

    var order = Math.max(Math.floor(Math.log(e.value) / Math.log(1000)), 0);
    if (order > suffixes.length - 1)
      order = suffixes.length - 1;

    var suffix = suffixes[order];
    return CanvasJS.formatNumber(e.value / Math.pow(1000, order)) + suffix;
  }

}