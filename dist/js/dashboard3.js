// Chart Inside Text 
Chart.pluginService.register({
  beforeDraw: function (chart) {
    if (chart.chart.chart.config.type == 'doughnut') {
      var width = chart.chart.width,
        height = chart.chart.height,
        ctx = chart.chart.ctx;
      ctx.restore();
      var fontSize = (height / 100).toFixed(2);
      ctx.font = fontSize + "em Product Sans";
      ctx.textBaseline = "middle";
      // Ensure your first data is the percentage data in your dataset
      var chart_percent = chart.chart.chart.data.datasets[0].data[0];
      chart_percent = chart_percent || 0;
      var text = chart_percent,
        textX = Math.round((width - ctx.measureText(text).width) / 2),
        textY = (height / 2)
      ctx.fillText(text, textX, textY);
      ctx.fillStyle = "#535353";
      ctx.save();
    }
  }
});
// Chart Inside Text Ends

// Pie Chart Start
var ctxPie = document.getElementById('pieChart');
var ctxPie1 = document.getElementById('pieChart1');
var ctxPie2 = document.getElementById('pieChart2');
var ctxPie3 = document.getElementById('pieChart3');
var ctxPie4 = document.getElementById('pieChart4');
var ctxPie5 = document.getElementById('pieChart5');

var pieChart = new Chart(ctxPie, {
  type: 'doughnut',
  data: {
    labels: ['Total Employees'],
    datasets: [{
      data: [150],
      backgroundColor: [
        'rgba(13, 143, 82, 0.6)',
        'rgba(13, 143, 82, 0.5)',
      ],
      display: true,
      labelString: 'Total Employees',
      borderColor: [
        'rgba(13, 143, 82, 1)',
      ],
      hoverBackgroundColor: ['rgba(13, 143, 82, 0.78)'],
      borderWidth: 1
    }]
  },
  options: {
    circular: true,
    cutoutPercentage: 70,
    legend: {
      display: false
    }
  }
});
var pieChart1 = new Chart(ctxPie1, {
  type: 'doughnut',
  data: {
    labels: ['Present Today', 'Absent Today'],
    datasets: [{
      data: [40, 110],
      backgroundColor: [
        'rgba(255, 145, 113, 0.8)',
        'rgba(255, 145, 113, 0.2)',
      ],
      borderColor: [
        'rgba(255, 145, 113, 1)',
      ],
      hoverBackgroundColor: ['rgba(255, 145, 113, 1)'],
      borderWidth: 1
    }]
  },
  options: {
    circular: true,
    cutoutPercentage: 70,
    legend: {
      display: false
    }
  }
});
var pieChart2 = new Chart(ctxPie2, {
  type: 'doughnut',
  data: {
    labels: ['Absent Today', 'Present Today'],
    datasets: [{
      data: [110, 40],
      backgroundColor: [
        'rgba(255, 113, 113, 0.8)',
        'rgba(255, 113, 113, 0.2)',
      ],
      borderColor: [
        'rgba(255, 113, 113, 1)',
      ],
      hoverBackgroundColor: ['rgba(255, 113, 113, 1)'],
      borderWidth: 1
    }]
  },
  options: {
    circular: true,
    cutoutPercentage: 70,
    legend: {
      display: false
    }
  }
});
var pieChart3 = new Chart(ctxPie3, {
  type: 'doughnut',
  data: {
    labels: ['Male', 'Female'],
    datasets: [{
      data: [75, 75],
      backgroundColor: [
        'rgba(255, 201, 60, 0.8)',
        'rgba(255, 201, 60, 0.2)',
      ],
      borderColor: [
        'rgba(255, 201, 60, 1)',
      ],
      hoverBackgroundColor: ['rgba(255, 201, 60,  1)'],
      borderWidth: 1
    }]
  },
  options: {
    circular: true,
    cutoutPercentage: 70,
    legend: {
      display: false
    }
  }
});
var pieChart4 = new Chart(ctxPie4, {
  type: 'doughnut',
  data: {
    labels: ['Female', 'Male'],
    datasets: [{
      data: [75, 75],
      backgroundColor: [
        'rgba(187, 153, 205, 0.8)',
        'rgba(187, 153, 205, 0.2)',
      ],
      borderColor: [
        'rgba(187, 153, 205, 1)',
      ],
      hoverBackgroundColor: ['rgba(187, 153, 205, 1)'],
      borderWidth: 1
    }]
  },
  options: {
    circular: true,
    cutoutPercentage: 70,
    legend: {
      display: false
    }
  }
});
var pieChart5 = new Chart(ctxPie5, {
  type: 'doughnut',
  data: {
    labels: ['Took Leave', 'Absent Today'],
    datasets: [{
      data: [80, 70],
      backgroundColor: [
        'rgba(224, 236, 228, 0.8)',
        'rgba(224, 236, 228, 0.2)',
      ],
      borderColor: [
        'rgba(224, 236, 228, 1)',
      ],
      hoverBackgroundColor: ['rgba(224, 236, 228, 1)'],
      borderWidth: 1
    }]
  },
  options: {
    circular: true,
    cutoutPercentage: 70,
    legend: {
      display: false
    }
  }
});

// Pie Chart Ends

// Line Chart Starts
var ctxLine = document.getElementById('lineChart').getContext('2d');

var lineChart1 = new Chart(ctxLine, {
  type: 'line',
  type: 'line',
  data: {
    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    datasets: [{
        label: 'Attended Female Employees',
        data: [75, 74, 40, 55, 60, 70, 71, 45, 53, 12, 44, 75],
        borderWidth: 2,
        borderColor: 'rgba(187, 153, 205, 1)',
        backgroundColor: 'rgba(187, 153, 205, 0.6)',
        borderWidth: 1,
        radius: 6,
        hoverRadius: 8,
        hoverBorderWidth: 1,
        fill: false,
        lineTension: 0.5,
        responsive: true
      },
      {
        label: 'Attended Male Employees',
        data: [50, 55, 40, 33, 22, 28, 40, 30, 34, 45, 51, 75],
        borderWidth: 2,
        borderColor: 'rgba(255, 201, 60, 1)',
        backgroundColor: 'rgba(255, 201, 60, 0.6)',
        borderWidth: 1,
        radius: 6,
        hoverRadius: 8,
        hoverBorderWidth: 1,
        fill: false,
        lineTension: 0.5,
      }
    ]
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: false
        }
      }]
    },
    legend: {
      display: false,
    }
  }
});
// Line Chart Ends

// bar chart starts
var ctxBar = document.getElementById('barChart').getContext('2d');

var barChart1 = new Chart(ctxBar, {
  type: 'bar',
  data: {
    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    datasets: [{
        label: 'Intern',
        data: [12, 19, 3, 17, 28, 24, 7, 15, 25, 35, 55, 1, 2],
        borderColor: 'rgba(187, 153, 205, 1)',
        backgroundColor: 'rgba(187, 153, 205, 0.6)',
        borderWidth: 1,
      },
      {
        label: 'Front End Developer',
        data: [70, 55, 23, 28, 60, 65, 10, 30, 44, 33, 22, 14, 12],
        borderColor: 'rgba(255, 201, 60, 1)',
        backgroundColor: 'rgba(255, 201, 60, 0.6)',
        borderWidth: 1,
      },
      {
        label: 'Back End Developer',
        data: [24, 33, 58, 64, 11, 22, 6, 64, 13, 28, 66, 13, 47],
        borderColor: 'rgba(255, 145, 113, 1)',
        backgroundColor: 'rgba(255, 145, 113, 0.6)',
        borderWidth: 1,
      },
      {
        label: 'Full Stack Developer',
        data: [23, 11, 5, 3, 1, 28, 6, 14, 11, 13, 60, 20, 50],
        borderColor: 'rgba(13, 143, 82, 1)',
        backgroundColor: 'rgba(13, 143, 82, 0.6)',
        borderWidth: 1,
      },
    ]
  },
  options: {
    legend: {
      display: false,
    }
  }
});
// bar chart ends