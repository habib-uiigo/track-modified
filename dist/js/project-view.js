window.onload = function onLoad() {
  var Line = new ProgressBar.Line('.project-view', {
    color: '#0D8F52',
    trailColor: '#ececec',
    trailWidth: 1,
    duration: 1400,
    easing: 'bounce',
    strokeWidth: 1,
    from: {
      color: '#0D8F52',
      a: 0
    },
    to: {
      color: '#0D8F52',
      a: 1
    },
    step: function (state, line) {
      line.path.setAttribute('stroke', state.color);
    }
  });
  Line.animate(.35);
}